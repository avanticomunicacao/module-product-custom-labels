<?php

namespace Avanti\ProductCustomLabels\Ui\Component\Listing\Grid\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class Position extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = 'label_position';
            foreach ($dataSource['data']['items'] as & $item) {
                switch ($item[$fieldName]) {
                    case 'top_left':
                        $item[$fieldName] = __("Top Left");
                        break;
                    case 'top_right':
                        $item[$fieldName] = __("Top Right");
                        break;
                    case 'lower_left':
                        $item[$fieldName] = __("Bottom Left");
                        break;
                    case 'lower_right':
                        $item[$fieldName] = __("Bottom Right");
                        break;
                }
            }
        }
        return $dataSource;
    }
}
