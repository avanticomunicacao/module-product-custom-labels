<?php

namespace Avanti\ProductCustomLabels\Ui\Component\Listing\Grid\Column;

use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class CategoryTreePath extends Column
{
    private $categoryRepository;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CategoryRepository $categoryRepository,
        array $components = [],
        array $data = []
    ) {
        $this->categoryRepository = $categoryRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = 'category_ids';
            foreach ($dataSource['data']['items'] as & $item) {
                $category = $this->categoryRepository->get($item[$fieldName]);
                $tree = $category->getPath();
                $categoryIdsPath = explode("/", $tree);
                $categoryTree = '';
                foreach ($categoryIdsPath as $cat) {
                    if ($cat == 1 || $cat == 2) {
                        continue;
                    }
                    $categoryCol = $this->categoryRepository->get($cat);
                    $categoryTree .= $categoryCol->getName() . '/';
                }
                $item[$fieldName] = $categoryTree;
            }
        }
        return $dataSource;
    }
}
