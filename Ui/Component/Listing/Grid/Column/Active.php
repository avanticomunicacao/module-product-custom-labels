<?php

namespace Avanti\ProductCustomLabels\Ui\Component\Listing\Grid\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class Active extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = 'label_active';
            foreach ($dataSource['data']['items'] as & $item) {
                if ($item[$fieldName] == 1) {
                    $item[$fieldName] = __('Yes');
                } else {
                    $item[$fieldName] = __('No');
                }
            }
        }
        return $dataSource;
    }
}
