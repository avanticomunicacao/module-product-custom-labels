<?php
namespace Avanti\ProductCustomLabels\Helper;

use Avanti\ProductCustomLabels\Model\ResourceModel\Label\CollectionFactory as LabelCollectionFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Data extends AbstractHelper
{
    const LABEL_TYPE = 'image';

    protected $product;
    protected $storeManageInterface;
    protected $labelCollectionFactory;
    protected $scopeConfig;

    /**
     * Data constructor.
     * @param ProductInterface $product
     * @param LabelCollectionFactory $labelCollectionFactory
     * @param StoreManagerInterface $storeManageInterface
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ProductInterface $product,
        LabelCollectionFactory $labelCollectionFactory,
        StoreManagerInterface $storeManageInterface,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->product = $product;
        $this->labelCollectionFactory = $labelCollectionFactory;
        $this->storeManageInterface = $storeManageInterface;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get List Labels Avaliable for Product
     * @param ProductInterface $product
     * @return array
     */
    public function getProductLabels(ProductInterface $product)
    {
        $labels = $this->labelCollectionFactory->create();
        $collection = $labels->addFieldToFilter('label_active', 1)->getData();
        $productLabels = [];

        $categoriesIds = $product->getCategoryIds();

        $discount = $this->calculateDiscount($product);
        if ($this->isActiveDiscountLabel() && $discount > 0) {
            $positon = $this->getConfig('discountlabel_position');
            $productLabels[$positon][0]['position'] = $positon;
            $productLabels[$positon][0]['label_type'] = 'discountLabel';
            $productLabels[$positon][0]['discount'] = $discount . '%';
        }

        foreach ($collection as $c) {
            $categoryId = $c['category_ids'];
            if (in_array($categoryId, $categoriesIds)) {
                $labelPosition = $c['label_position'];
                $productLabels[$labelPosition][$c['label_id']]['image'] = $c['label_image'];
                $productLabels[$labelPosition][$c['label_id']]['position'] = $c['label_position'];
                $productLabels[$labelPosition][$c['label_id']]['label_type'] = self::LABEL_TYPE;
            }
        }

        return $productLabels;
    }

    /**
     * @param ProductInterface $product
     * @return $this
     */
    public function setProduct(ProductInterface $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get Image Url
     * @param $imageName
     * @return string
     */
    public function getLabelUrl($imageName)
    {
        $mediaUrl = $this->storeManageInterface->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl . 'productlabels/label/' . $imageName;
    }

    /**
     * Return Configuration of the module
     * @param $field
     * @return mixed
     */
    public function getConfig($field)
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('avanti_productlabels/general/' . $field, $storeScope);
    }

    /**
     * Check if Discount Label is Enabled
     * @return int
     */
    public function isActiveDiscountLabel()
    {
        return $this->getConfig('discountlabel_active');
    }

    /**
     * Return Percent Discount
     * @param ProductInterface $product
     * @return int
     */
    public function calculateDiscount($product)
    {
        $productPrice = $product->getData('price');
        $discountPrice = $product->getSpecialPrice();
        $billetPrice = $this->getBilletPrice($discountPrice);

        if ($this->isDiscountBasedBillet()) {
            return intval((($productPrice - $billetPrice)/$productPrice) * 100);
        }
        if ($discountPrice) {
            if ($discountPrice > $productPrice) {
                return 0;
            }
            return intval((($productPrice - $discountPrice) / $productPrice) * 100);
        }
        return 0;
    }

    public function isDiscountBasedBillet()
    {
        return $this->getConfig('percetage_discount_based_billet');
    }

    public function getDiscountBillet()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('payment/mercadopago_customticket/percent_discount', $storeScope);
    }

    public function getBilletPrice($specialPrice)
    {
        return $specialPrice - (($this->getDiscountBillet() * $specialPrice ) / 100);
    }
}
