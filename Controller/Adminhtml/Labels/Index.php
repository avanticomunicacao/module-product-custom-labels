<?php


namespace Avanti\ProductCustomLabels\Controller\Adminhtml\Labels;


use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }


    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Avanti_ProductCustomLabels::plabelmenu');
        $resultPage->getConfig()->getTitle()->prepend((__('Manage Labels')));

        return $resultPage;
    }
}
