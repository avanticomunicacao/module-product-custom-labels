<?php

namespace Avanti\ProductCustomLabels\Controller\Adminhtml\Labels;

use Avanti\ProductCustomLabels\Model\LabelFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page as ResultPage;
use Magento\Framework\Controller\ResultFactory;

class Edit extends Action
{
    protected $labelFactory;

    public function __construct(
        Action\Context $context,
        LabelFactory $labelFactory
    ) {
        $this->labelFactory = $labelFactory;
        parent::__construct($context);
    }

    /**
     * @return ResultPage
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('label_id');
        $label = $this->labelFactory->create();

        if ($id) {
            $label->load($id);
        }

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->getConfig()->getTitle()->prepend(__('Labels'));
        $resultPage->getConfig()->getTitle()->prepend(
            $label->getLabelId() ? __('Edit Label %1', $label->getLabelId()) : __('New Label')
        );

        return $resultPage;
    }
}
