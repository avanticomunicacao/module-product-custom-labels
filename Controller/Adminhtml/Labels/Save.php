<?php
namespace Avanti\ProductCustomLabels\Controller\Adminhtml\Labels;

use Avanti\ProductCustomLabels\Model\ImageUploader;
use Avanti\ProductCustomLabels\Model\LabelFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Cache\TypeList as CacheTypeList;

class Save extends Action
{
    protected $labelFactory;
    protected $imageUploader;
    protected $cache;

    /**
     * Save constructor.
     * @param Context $context
     * @param LabelFactory $labelFactory
     * @param ImageUploader $imageUploader
     * @param CacheTypeList $cache
     */
    public function __construct(
        Context $context,
        LabelFactory $labelFactory,
        ImageUploader $imageUploader,
        CacheTypeList $cache
    ) {
        $this->labelFactory = $labelFactory;
        $this->imageUploader = $imageUploader;
        $this->cache = $cache;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        try {
            if ($data) {
                $label = $this->labelFactory->create();
                if ($data['label_id']) {
                    $label->load($data['label_id']);
                }
                $name = isset($data['image'][0]['image']) ? $data['image'][0]['image'] : null;

                if ($data['image'] && isset($data['image'][0]['name'])) {
                    $name = $data['image'][0]['name'];
                    $this->imageUploader->moveFileFromTmp($name);
                }
                $label->setData('label_name', $data['label_name']);
                $label->setData('label_position', $data['label_position']);
                $label->setData('label_active', $data['label_active']);
                $label->setData('label_image', $name ? $name : $data['image'][0]['name']);
                $label->setData('category_ids', $data['category_ids']);
                $label->save();
                $this->messageManager->addSuccessMessage(__("Label successfully saved"));
                $this->cache->cleanType('full_page');
                $this->cache->cleanType('block_html');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Error saving Label"));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
