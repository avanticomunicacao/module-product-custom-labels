<?php
namespace Avanti\ProductCustomLabels\Controller\Adminhtml\Labels;

use Avanti\ProductCustomLabels\Model\LabelFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context as BackendContext;
use Magento\Framework\App\Cache\TypeList as CacheTypeList;

class Delete extends Action
{
    protected $cache;
    protected $labelFactory;

    /**
     * Delete constructor.
     * @param BackendContext $context
     * @param LabelFactory $labelFactory
     * @param CacheTypeList $cache
     */
    public function __construct(
        BackendContext $context,
        LabelFactory $labelFactory,
        CacheTypeList $cache
    ) {
        $this->labelFactory = $labelFactory;
        $this->cache = $cache;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('label_id');

        if ($id) {
            try {
                $label = $this->labelFactory->create()->load($id);
                $label->delete();
                $this->messageManager->addSuccessMessage(__("Label successfully deleted"));
                $this->cache->cleanType('full_page');
                $this->cache->cleanType('block_html');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__("Error deleting the Label"));
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
