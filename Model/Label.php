<?php
namespace Avanti\ProductCustomLabels\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Label extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'avanti_productcustomlabel_label';

    protected $_cacheTag = 'avanti_productcustomlabel_label';
    protected $_eventPrefix = 'avanti_productcustomlabel_label';

    public function _construct()
    {
        $this->_init("Avanti\ProductCustomLabels\Model\ResourceModel\Label");
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        return [];
    }
}
