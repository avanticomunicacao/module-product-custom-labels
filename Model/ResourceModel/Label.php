<?php
namespace Avanti\ProductCustomLabels\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Label extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('avanti_productlabel', 'label_id');
    }
}
