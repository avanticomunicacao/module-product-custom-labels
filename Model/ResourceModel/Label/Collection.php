<?php
namespace Avanti\ProductCustomLabels\Model\ResourceModel\Label;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init("Avanti\ProductCustomLabels\Model\Label", "Avanti\ProductCustomLabels\Model\ResourceModel\Label");
    }
}