<?php
namespace Avanti\ProductCustomLabels\Model;

use Avanti\ProductCustomLabels\Model\ResourceModel\Label\CollectionFactory;
use Magento\Framework\UrlInterface as UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    protected $loadData;
    protected $storeManager;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $labelCollectionFactory,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $labelCollectionFactory->create();
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadData)) {
            return $this->loadData;
        }

        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->loadData[$item->getId()] = $item->getData();

            $data = $item->getData();

            if (isset($data['label_image'])) {
                $baseurl =  $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                $img = [];
                $img[0]['image'] = $data['label_image'];
                $img[0]['url'] = $baseurl . 'productlabels/label/' . $data['label_image'];
                $data['image'] = $img;
            }
            $this->loadData[$item->getId()] = $data;
        }
        return $this->loadData;
    }
}
