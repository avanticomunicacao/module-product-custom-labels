<?php
namespace Avanti\ProductCustomLabels\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class DiscountLabelPosition implements ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'top_left', 'label' => __('Top Left')],
            ['value' => 'top_right', 'label' => __('Top Right')],
            ['value' => 'lower_left', 'label' => __('Bottom Left')],
            ['value' => 'lower_right', 'label' => __('Bottom Right')]];
    }

    public function toArray()
    {
        return [
            'top_left' => __('Top Left'),
            'top_right' => __('Top Left'),
            'lower_left' => __('Bottom Left'),
            'lower_right' => __('Bottom Right')
        ];
    }
}
