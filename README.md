# Product Custom Lablels
Com esse módulo é possível gerenciar as labels
que são exibidas nos produtos. As labels são imagens,
que "flutuam" sobre a imagem do produto.

## Instalação
Adicione o seguinte código no node repositories do
composer.json do projeto:

```
"mgt-product-custom-labels": {
    "url": "https://bitbucket.org/avanticomunicacao/mgt-product-custom-labels.git",
    "type": "git"
}
```

Depois adicione o seguinte trecho no node require do composer.json

```"penseavanti/mgt-product-custom-labels": "1.0.4"```

Rode composer install para que a instalação seja feita

## Uso

### Listando as Labels Cadastradas
O Módulopossui uma área no painel administrativo onde é possível fazer
o cadastro das labels. Para acess-lo, acesse a área administrativa do site,
e pelo menu lateral, vá até ***Avanti -> Product Label***. Conforme Exibido
na imagem abaixo:
![Image of Settings](docs/access_module.png)
Na tela será exibida uma grid com todas as label cadastradas.

### Cadastrando novas Labels
Na tela de listagem de labels, clique sobre o botão **Add Nova Label**. Assim,
você será redirecionado a tela de cadastro, conforme imagem abaixo:
![Image of New Label](docs/tela_cadastro.png)
Os campos que serão requiridos são:
1. **Nome**: Nome Informativo da Label (usado para controle).
2. **Habilitar Label**: Se a Label irá ou não ser exibida.
3. **Posição**: Em qual canto da imagem será exibida a label.
as opções são: **Superior Esquerdo**, **Superior Direito**, **Inferior Esquerdo** e **Inferior Direito**.
4. **Categoria**: somente produtos atrelados a categoria escolhida que exibirão a label.
5. **Imagem**: A imagem da label em sí.

O exemplo abaixo foi criado:
![Image of New Label](docs/tela_cadastro_preenchido.png)

Ao salvar a label será criada e ela aparecerá na grid. Conforme imagem abaixo:
![Image of list All Labels](docs/listagem_labels_cadastradas.png)

### Exibição da Label na loja
Conforme o exemplo criado, ao acessar a categoria (Gear -> Watches), que foi a escolhida,
a label irá aparecer em todos os produtos na parte **superior esquerda**, conforme imagem:
![Image of Details Product Category](docs/listagem_produtos_categoria.png)

E ao acessar o produto, a label também será exibida no mesma posição da listagem. Conforme imagem abaixo:

![Image of Details Product View](docs/listagem_produto_detalhes.png)

### Configurações do Módulo
No módulo é possível exibir uma label com a porcentagem de desconto de produto.
Para habilitar essa funcionalidade, acesse o painel administrativo da loja,
e navegue até: **Loja -> Configuração -> Avanti -> Product Labels**. A seguinte tela
irá aparecer:

![Image of Configuration Label Discount](docs/product_labels_configuration.png)

Existem duas opções de configuração, são elas:

**Habilitar Label de Desconto?**: com essa opção é possível exibir ou não a label
de porcentagem de desconto.

**Posição da Label de Desconto**: neste campo, é possível escolher a posição que a label
irá ficar na tela. Tem as seguintes posições:
1. Superior Esquerdo
2. Superior Direito
3. Inferior Esquerdo
4. Inferior Direito

Após habilitar a opção de exibir o label de porcentagem de desconto, e escolher
a posição, a label será exibida em todos os pordutos que possuem **preço especial (special price)**. 
Conforme mostrado na imagem abaixo:

![Image of Configuration Label Discount](docs/label_desconto_product.png)

No exemplo, foi escolhida a posição **Inferior Direito**

E ao acessar o produto, a label também será exibida no mesma posição
da listagem. Conforme imagem abaixo:

![Image Product View Discount Label](docs/discount_percent_product_view.png)
