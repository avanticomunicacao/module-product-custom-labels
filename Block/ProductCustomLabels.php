<?php
namespace Avanti\ProductCustomLabels\Block;

use Avanti\ProductCustomLabels\Helper\Data as LabelHelper;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Registry;

class ProductCustomLabels extends Template
{
    private $storeManageInterface;
    private $labelHelper;
    private $registry;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManageInterface,
        LabelHelper $labelHelper,
        Registry $registry,
        array $data = []
    ) {
        $this->storeManageInterface = $storeManageInterface;
        $this->labelHelper = $labelHelper;
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function getLabels()
    {
        $product = $this->getCurrentProduct();
        return $this->labelHelper->getProductLabels($product);
    }

    public function getLabelUrl($imageName)
    {
        $mediaUrl = $this->storeManageInterface->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl . 'productlabels/label/' . $imageName;
    }

    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }
}
