<?php
namespace Avanti\ProductCustomLabels\Block\Adminhtml\Component\Edit;

use Magento\Backend\Block\Widget\Context as WidgetContext;

abstract class AbstractButton
{
    protected $context;

    /**
     * @param WidgetContext $context
     */
    public function __construct(WidgetContext $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID.
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->context->getRequest()->getParam('component_id');
    }

    /**
     * Generate url by route and parameters.
     *
     * @param string $route
     * @param array  $params
     *
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
